import basic.beans.SampleStatelessBean;
import basic.entities.*;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.embeddable.EJBContainer;
import javax.inject.Inject;
import javax.naming.NamingException;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.*;
import javax.transaction.RollbackException;
import java.util.*;

/**
 * Created by Ganesh on 11-05-2014.
 */

public class TestEmployee {

    @PersistenceContext(unitName = "employee-unit",type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Resource
    private UserTransaction userTransaction;

    @EJB
    private SampleStatelessBean sampleStatelessBean;




    @Before
    public void setUp() {

        final Map p = new Properties();
        try {
            EJBContainer.createEJBContainer().close();
            EJBContainer.createEJBContainer(p).getContext().bind("inject", this);
        } catch (NamingException e) {
            Assert.fail(" failed to init test case with openejb container " + e.getLocalizedMessage());
        }
    }


    @Test
    public void testQL() throws HeuristicMixedException, RollbackException, SystemException, NamingException, HeuristicRollbackException, NotSupportedException {

        testEmpDb();

        userTransaction.begin();
        List< ? extends Employee> employees=entityManager.createQuery("select e from Employee e",Employee.class).getResultList();
        displayEes(employees);

        List<String> names=entityManager.createQuery("select distinct e.name from Employee e",String.class).getResultList();
        for(String name:names)
        {
            System.out.println(name);
        }

        employees=entityManager.createQuery("select  NEW basic.entities.Employee(e.id,e.name,e.salary) from Employee e",Employee.class).getResultList();
        detachedDisplay(employees);
        /*employees =entityManager.createQuery("select e from Employee e where TYPE(e)=Employee",Employee.class).getResultList();
        displayEes(employees); NO INHERITANCE !!
        */

        List<? extends  Project> projects=entityManager.createQuery("select p from Employee e JOIN e.projects p",Project.class).getResultList();
        displayPjs(projects);
        List<? extends  PhoneEntry> phoneEntries=entityManager.createQuery("select VALUE(mpe) from Employee e JOIN e.phoneEntryMap mpe").getResultList();
        displayPes(phoneEntries);

        employees=entityManager.createQuery("select e from Employee e JOIN FETCH  e.vacations vac ",Employee.class).getResultList();
        detachedDisplay(employees);
        employees=entityManager.createQuery("select e from Employee e where :project  MEMBER OF e.projects ",Employee.class).setParameter("project",projects.get(0)).getResultList();
        displayEes(employees);
        int i = entityManager.createQuery("UPDATE Employee e set e.salary=e.salary+500 where 1=1").executeUpdate();
        Assert.assertTrue(i>0);
        List<String> resultList = entityManager.createQuery("select CASE " +
                "WHEN KEY(e.phoneEntryMap) = 'HOME' THEN 'home' " +
                "WHEN KEY(e.phoneEntryMap)='MOBILE_PERSONAL' THEN 'Personal Mobile'" +
                "WHEN KEY(e.phoneEntryMap)='MOBILE_PRIVATE' THEN 'Private Mobile'" +
                "WHEN KEY(e.phoneEntryMap)='WORK' THEN 'Work'" +
                "WHEN KEY(e.phoneEntryMap)='EMERGENCY' THEN 'Emergency'" +
                "END " +
                "FROM Employee e", String.class).getResultList();
        // does not work due to CHAR padding in hSQL Assert.assertTrue(resultList.contains("home"));
        Assert.assertTrue(resultList.contains("Personal Mobile"));

        userTransaction.commit();
    }

    @Test
    public void testAddCriterias() throws HeuristicMixedException, RollbackException, SystemException, NamingException, HeuristicRollbackException, NotSupportedException {

        testEmpDb();
        userTransaction.begin();
        CriteriaBuilder cb =entityManager.getCriteriaBuilder();
        CriteriaQuery<String> query = cb.createQuery(String.class);
        Root<Employee> emp=query.from(Employee.class);
        query.select(emp.<String>get("name")).where(cb.equal(emp.get("name"),"FirstEmployee"));


        userTransaction.commit();

    }

    private void detachedDisplay(List<? extends Employee> employees) {
        for(Employee e : employees)
        {
            entityManager.detach(e);
            System.out.println(e);
        }
    }

    private void displayPes(List<? extends PhoneEntry> phoneEntries) {
        for(PhoneEntry pe:phoneEntries)
            System.out.println(pe);
    }

    private void displayPjs(List<? extends Project> projects) {
        for(Project project:projects)
            System.out.println(project);
    }

    private void displayEes(List<? extends Employee> employees) {
        for(Employee employee:employees){
            System.out.println(employee);
        }
    }


    @Test
    public void testSampleStatelessBean() throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
       userTransaction.begin();
       Department department = createITDept(1, entityManager, DepartmentName.IT);
       Project prj1 = createProject("ONE", 1);
       Address address = createAddress("one", "two", "ZIP");
       Employee emp = createEmployee(department, address, prj1, null);
       entityManager.persist(emp);
       entityManager.flush();
       sampleStatelessBean.assignEmployeeToProject(emp.getId(), prj1.getId());
       entityManager.refresh(prj1);
       Assert.assertEquals("project should contain the employee", prj1.getEmployees().iterator().next(), emp);
       userTransaction.commit();
    }



    @Test
    public void testEmpDb() throws NamingException, SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        userTransaction.begin();
        Department department = createITDept(1, entityManager, DepartmentName.IT);
        createParkingSpace(1, entityManager);
        Address address = createAddress("one", "two", "ZIP");

        Project prj1 = createProject("ONE", 1);
        entityManager.persist(prj1);
        ParkingSpace parkingSpace = entityManager.find(ParkingSpace.class, 1);
        Assert.assertNotNull(parkingSpace);
        VacationEntry vacationEntry = createVacEntry(0l, 1l, VacationType.PRIVATE);


        Employee emp = createEmployee(department, address, prj1, parkingSpace);
        emp.setVacations(new HashSet<VacationEntry>());
        emp.getVacations().add(vacationEntry);
        PhoneEntry phoneEntry = createPhoneEntry(emp, 1, "!2324", PhoneType.HOME);
        PhoneEntry phoneEntry1=createPhoneEntry(emp,2,"123",PhoneType.MOBILE_PERSONAL);
        entityManager.persist(emp);
        entityManager.flush();
        entityManager.persist(phoneEntry);
        entityManager.persist(phoneEntry1);
        PrintJob job = createPrintJob(emp, 1);
        PrintQueue queue = createPrintQueue(job);
        entityManager.persist(job);
        entityManager.persist(queue);
        entityManager.flush();
        job=entityManager.find(PrintJob.class,1);
        Assert.assertNotNull("job must've been attached to queue",job.getQueue());

        Employee employee = entityManager.find(Employee.class, 123);
        entityManager.refresh(employee);
        Assert.assertNotNull("Persisted entity shouldn't be null!",employee);
        parkingSpace=entityManager.find(ParkingSpace.class,1);
        Map<PhoneType, PhoneEntry> phoneEntryMap = employee.getPhoneEntryMap();
        Assert.assertEquals(phoneEntryMap.size(),2);
        Assert.assertEquals(phoneEntryMap.containsKey(PhoneType.HOME),true);
        Assert.assertEquals(phoneEntryMap.containsKey(PhoneType.MOBILE_PERSONAL),true);
        entityManager.refresh(parkingSpace);
        Assert.assertEquals(emp, employee);
        entityManager.refresh(prj1);
        Assert.assertNotNull(prj1.getEmployees().iterator().next());
        VacationEntry next = employee.getVacations().iterator().next();
        Assert.assertNotNull(next);
        Assert.assertEquals(next,vacationEntry);
        Assert.assertNotNull("One to One mapping for employee in parking space should not be null",parkingSpace.getEmployee());

        Dependent dependent= new Dependent();
        dependent.setEmp(emp);
        dependent.setName("Mother");
        entityManager.persist(dependent);
        entityManager.flush();
        entityManager.refresh(emp);
        Dependent dep1 = emp.getDependents().iterator().next();
        Assert.assertEquals(dep1,dependent);

        userTransaction.commit();
    }

    private PhoneEntry createPhoneEntry(Employee emp, int id, String number, PhoneType home) {
        PhoneEntry phoneEntry= new PhoneEntry();
        phoneEntry.setId(id);
        phoneEntry.setNumber(number);
        phoneEntry.setType(home);
        phoneEntry.setEmployee(emp);
        return phoneEntry;
    }

    private PrintQueue createPrintQueue(PrintJob job) {
        PrintQueue queue= new PrintQueue();
        queue.setId(1);
        queue.setJobs(new HashSet<PrintJob>());
        queue.getJobs().add(job);
        job.setQueue(queue);
        return queue;
    }

    private PrintJob createPrintJob(Employee emp, int jobId) {
        PrintJob job= new PrintJob();
        job.setId(jobId);
        job.setEmployee(emp);
        return job;
    }

    private VacationEntry createVacEntry(long startDate, long endDate, VacationType vacationType) {
        VacationEntry vacationEntry = new VacationEntry();
        vacationEntry.setStartDate(new Date(startDate));
        vacationEntry.setEndDate(new Date(endDate));
        vacationEntry.setVacationType(vacationType);
        return vacationEntry;
    }

    private Employee createEmployee(Department department, Address address, Project prj1, ParkingSpace parkingSpace) {
        Employee emp = new Employee();
        emp.setId(123);
        emp.setType(EmployeeType.CONTRACT);
        emp.setName("FirstEmployee");
        emp.setSalary(2345l);
        emp.setProjects(new HashSet<Project>());
        emp.getProjects().add(prj1);
        emp.setAddress(address);
        emp.setParkingSpace(parkingSpace);
        emp.setDepartment(department);
        EmploymentPeriod perio= new EmploymentPeriod();
        perio.setStartDate(new Date());
        perio.setEndDate(new Date());
        emp.setEmploymentPeriod(perio);
        return emp;
    }

    private Address createAddress(String one, String two, String zip) {
        Address address= new Address();
        address.setOne(one);
        address.setTwo(two);
        address.setZip(zip);
        return address;
    }

    private Project createProject(String name, int id) {
        Project prj1 = new Project();
        prj1.setName(name);
        prj1.setId(id);
        return prj1;
    }

    private void createParkingSpace(int id, EntityManager localManager) {
        ParkingSpace space = new ParkingSpace();
        space.setId(id);
        if(localManager.find(ParkingSpace.class,id)==null)
            localManager.persist(space);
    }

    private Department createITDept(int id, EntityManager localManager, DepartmentName dept) {
        Department department= new Department();
        department.setId(id);
        department.setDepartmentName(dept);
        if(localManager.find(Department.class,id)==null)
            localManager.persist(department);
        return department;
    }

    @Test
    public void testPlayingWithQueriesOnEmp() throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        userTransaction.begin();
        Department department = createITDept(1, entityManager, DepartmentName.IT);
        createParkingSpace(1, entityManager);
        Employee emp = new Employee();
        emp.setId(123);
        emp.setName("FirstEmployee");
        emp.setSalary(1245l);
        emp.setDepartment(department);
        entityManager.persist(emp);
        TypedQuery<? extends Employee> tqp=entityManager.createQuery("select e from Employee e", Employee.class);
        List<? extends Employee> resultList = tqp.getResultList();
        Assert.assertEquals(resultList.size(),1);
        Query nativeQuery = entityManager.createNativeQuery("select * from Emp", Employee.class);
        Object singleResult = nativeQuery.getSingleResult();
        Assert.assertNotNull(singleResult);
        Assert.assertEquals(singleResult,resultList.get(0));
        userTransaction.commit();

    }

}

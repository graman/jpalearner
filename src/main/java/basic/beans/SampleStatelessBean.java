package basic.beans;

import basic.entities.Employee;
import basic.entities.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Ganesh on 25-05-2014.
 */
@Stateless
public class SampleStatelessBean {

    @PersistenceContext(unitName = "employee-unit",type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    public void assignEmployeeToProject(int emplId, int projId) {

        Employee employee = em.getReference(Employee.class, emplId);
        Project project = em.getReference(Project.class, projId);

        if (employee.getProjects() != Collections.EMPTY_SET) {
            employee.getProjects().add(project);
            em.persist(employee);
        } else {
            HashSet<Project> projects = new HashSet<Project>();
            projects.add(project);
            employee.setProjects(projects);
            em.persist(employee);
        }
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}

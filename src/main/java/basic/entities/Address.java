package basic.entities;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by Ganesh on 20-05-2014.
 */
@SuppressWarnings({"PublicMethodNotExposedInInterface", "InstanceVariableMayNotBeInitialized"})
@Embeddable
@Access(AccessType.PROPERTY)
public class Address {

    @Column(name = "EMP_ADD1")
    private String one;
    @Column(name = "EMP_ADD2")
    private String two;
    @Column(name = "EMP_ZIP")
    private String zip;





    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}

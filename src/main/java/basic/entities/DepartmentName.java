package basic.entities;

/**
 * Created by Ganesh on 19-05-2014.
 */
public enum DepartmentName {
    IT,
    ADMIN,
    FINANCE,
    LEGAL

}

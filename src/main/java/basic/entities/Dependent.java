package basic.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ganesh on 07-11-2014.
 */
@Entity
@Table(name = "DPNDT")
@IdClass(DependentId.class)
@Access(AccessType.PROPERTY)
public class Dependent implements Serializable {

    @Id
    @Column(name = "NAME")
    private  String name;



     private  Employee emp;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Id @ManyToOne
    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }
}

package basic.entities;

import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ganesh on 20-05-2014.
 */
@SuppressWarnings({"PublicMethodNotExposedInInterface", "InstanceVariableMayNotBeInitialized"})
@Embeddable
@Access(AccessType.PROPERTY)
public class VacationEntry {

    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;
    @Temporal(TemporalType.DATE)
    private Date endDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public VacationType getVacationType() {
        return vacationType;
    }

    public void setVacationType(VacationType vacationType) {
        this.vacationType = vacationType;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VacationEntry that = (VacationEntry) o;
        endDate=DateUtils.truncate(endDate, Calendar.HOUR);
        startDate=DateUtils.truncate(startDate,Calendar.HOUR);
        that.startDate=DateUtils.truncate(that.startDate,Calendar.HOUR);
        that.endDate=DateUtils.truncate(that.endDate,Calendar.HOUR);
        //noinspection SimplifiableIfStatement
        if (!endDate.equals(that.endDate)) return false;
        return startDate.equals(that.startDate) && vacationType == that.vacationType;

    }

    @Override
    public int hashCode() {
        int result = startDate.hashCode();
        result = 31 * result + vacationType.hashCode();
        result = 31 * result + endDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "VacationEntry{" +
                "startDate=" + startDate +
                ", vacationType=" + vacationType +
                ", endDate=" + endDate +
                '}';
    }
}

package basic.entities;


/**
 * Created by Ganesh on 25-05-2014.
 */

public enum PhoneType {
    HOME,
    MOBILE_PERSONAL,
    MOBILE_PRIVATE,
    WORK,
    EMERGENCY
}

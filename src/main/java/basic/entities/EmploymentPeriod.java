package basic.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Ganesh on 07-11-2014.
 */
@Embeddable
public class EmploymentPeriod {

    private Date startDate;


    private Date endDate;


    @Column(name = "START_DT")
    @Temporal(TemporalType.DATE)
    @Access(AccessType.PROPERTY)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    @Access(AccessType.PROPERTY)
    @Column(name = "END_DT")
    @Temporal(TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}

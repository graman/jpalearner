package basic.entities;

import java.io.Serializable;

/**
 * Created by Ganesh on 07-11-2014.
 */

public class DependentId  implements Serializable {
    private  String name;
    private  Integer emp;

    public DependentId(String name, Integer emp) {
        this.name = name;
        this.emp = emp;
    }

    public DependentId(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmp() {
        return emp;
    }

    public void setEmp(Integer emp) {
        this.emp = emp;
    }
}

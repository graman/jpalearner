package basic.entities;


import javax.persistence.*;
import java.util.*;

/**
 *
 * Employee stored in EMP table
 */
@SuppressWarnings({"MethodReturnOfConcreteClass", "PublicMethodNotExposedInInterface", "InstanceVariableOfConcreteClass", "ClassWithTooManyFields", "ClassWithTooManyMethods", "CyclicClassDependency", "ReturnOfCollectionOrArrayField", "InstanceVariableMayNotBeInitialized"})
@Entity
@Table(name = "EMP")
public class Employee {


    @Id
    @Column(name = "EMP_ID")
    private Integer id;

    @Column(name = "EMP_NAME")
    private String name;
    @Column(name = "EMP_SAL")
    private long salary;

    @Column(name = "EMP_DESC")
    @Basic(fetch = FetchType.LAZY)
    private String comments;

    @Lob @Column(name = "PIC") @Basic(fetch = FetchType.LAZY)
    private byte[] picture;

    @Column(name = "EMP_TYPE")
    @Enumerated(EnumType.STRING)
    private EmployeeType type;


    @Column(name = "EMP_START_DATE")
    @Basic(fetch = FetchType.LAZY)
    @Temporal(TemporalType.DATE)
    private Date startDate;


    @SuppressWarnings("FieldCanBeLocal")
    transient private String garbledName;

    @Column (name="EMP_GID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int gid;

    @ManyToOne
    @JoinColumn(name = "EMP_DPT_ID",nullable = false)
    private Department department;

    @OneToMany(mappedBy = "emp")
    @Basic(fetch = FetchType.LAZY)
    private Set<Dependent> dependents;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.MERGE})
    @JoinColumn(name = "EMP_PRK_SPC",nullable = true)
    private ParkingSpace parkingSpace;


    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.MERGE})
    @JoinTable(name = "EMP_PRJ",joinColumns = @JoinColumn(name = "EMP_ID"),inverseJoinColumns = @JoinColumn(name="PRJ_ID"))
    private Collection<Project> projects = Collections.EMPTY_SET;

    @Embedded
    @AttributeOverrides(
            {@AttributeOverride(name = "one",column = @Column(name = "EMP_ADD1"))}
    )
    private Address address;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "EMP_VAC",joinColumns = {@JoinColumn(name = "EMP_ID")} )
    @AttributeOverrides(
            {
                    @AttributeOverride(name = "startDate",column = @Column(name = "STRT_DT")),
                    @AttributeOverride(name = "endDate",column = @Column(name = "END_DT")),
                    @AttributeOverride(name = "vacationType",column = @Column(name = "VAC_TYPE"))
            }
    )
    private Collection<VacationEntry> vacations= Collections.EMPTY_SET;

    @OneToMany(mappedBy = "employee")
    private Collection<PrintJob> jobs;

    @OneToMany(mappedBy = "employee")
    @MapKeyColumn(name = "PHN_TYPE")
    @MapKeyEnumerated(EnumType.STRING)
    private Map<PhoneType,PhoneEntry> phoneEntryMap;


    public Employee() {
    }

    public Employee(Integer id, String name, long salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startDate",column = @Column(name = "START_DT")),
            @AttributeOverride(name = "endDate",column = @Column(name = "END_DT"))
    })
    private EmploymentPeriod employmentPeriod;

    public Employee(int id) {
        this.id = id;
    }

    /**
     * @return identifier of the corresponding employee
     */
    public int getId() {
        return id;
    }

    /**
     * Set the identifier given as id
     * @param id Identifier
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return name of employee
     */
    @Access(AccessType.PROPERTY)
    public String getName() {
        return name;
    }

    /**
     * Set the name of the identifier
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return salary of employee
     */
    @Access(AccessType.PROPERTY)
    public long getSalary() {
        return salary;
    }

    /**
     * Set salary of employee
     * @param salary salary
     */
    public void setSalary(long salary) {
        this.salary = salary;
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @param o the other object
     * @return true if equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        return id.equals(employee.id);

    }

    /**
     * @see Object#hashCode()
     * @return int code as hashed for this object
     */

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /**
     *
     * @return comments as description of the employee
     */
    @Access(AccessType.PROPERTY)
    public String getComments() {
        return comments;
    }

    /**
     * Set comments as description of an employee
     * @param comments comments
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     *
     * @return picture as bytes
     */
    @Access(AccessType.PROPERTY)
    public byte[] getPicture() {
        return picture;
    }

    /**
     *
     * @param picture as bytes
     */
    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    /**
     *
     * @return type of employee
     */
    @Access(AccessType.PROPERTY)
    public EmployeeType getType() {
        return type;
    }

    /**
     *
     * @param type set type of employee
     */
    public void setType(EmployeeType type) {
        this.type = type;
    }

    /**
     *
     * @return start Date of employee
     */
    @Access(AccessType.PROPERTY)
    public Date getStartDate() {
        return startDate;
    }

    /**
     *
     * @param startDate for setting the start date of this employee
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @SuppressWarnings("SameReturnValue")
    public String getGarbledName() {
        return "";
    }

    public void setGarbledName(String garbledName) {
        this.garbledName = garbledName;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public ParkingSpace getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(ParkingSpace parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public Collection<Project> getProjects() {
        return projects;
    }

    public void setProjects(Collection<Project> projects) {
        this.projects = projects;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Collection<VacationEntry> getVacations() {
        return vacations;
    }

    public void setVacations(Collection<VacationEntry> vacations) {
        this.vacations = vacations;
    }

    public Collection<PrintJob> getJobs() {
        return jobs;
    }

    public void setJobs(Collection<PrintJob> jobs) {
        this.jobs = jobs;
    }


    public Map<PhoneType, PhoneEntry> getPhoneEntryMap() {
        return phoneEntryMap;
    }

    public void setPhoneEntryMap(Map<PhoneType, PhoneEntry> phoneEntryMap) {
        this.phoneEntryMap = phoneEntryMap;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", comments='" + comments + '\'' +
                ", picture=" + Arrays.toString(picture) +
                ", type=" + type +
                ", startDate=" + startDate +
                ", garbledName='" + garbledName + '\'' +
                ", gid=" + gid +
                ", department=" + department +
                ", parkingSpace=" + parkingSpace +
                ", projects=" + projects +
                ", address=" + address +
                ", vacations=" + vacations +
                ", jobs=" + jobs +
                ", phoneEntryMap=" + phoneEntryMap +
                '}';
    }

    public Set<Dependent> getDependents() {
        return dependents;
    }

    public void setDependents(Set<Dependent> dependents) {
        this.dependents = dependents;
    }

    public EmploymentPeriod getEmploymentPeriod() {
        return employmentPeriod;
    }

    public void setEmploymentPeriod(EmploymentPeriod employmentPeriod) {
        this.employmentPeriod = employmentPeriod;
    }
}

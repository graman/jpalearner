package basic.entities;

/**
 * Created by Ganesh on 20-05-2014.
 */
public enum VacationType {
    PRIVATE,
    SICK,
    PRIVILEGED,
    UNKNOWN,
    OTHER

}

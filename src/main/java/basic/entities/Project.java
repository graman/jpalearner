package basic.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Ganesh on 20-05-2014.
 */
@SuppressWarnings({"PublicMethodNotExposedInInterface", "TypeMayBeWeakened", "CyclicClassDependency", "ReturnOfCollectionOrArrayField", "InstanceVariableMayNotBeInitialized"})
@Entity
@Table(name = "PROJ")
public class Project {

    @Column(name = "PRJ_NAME")
    private String name;
    @Id
    @Column(name = "PRJ_ID")
    private int id;

    @Basic(fetch = FetchType.LAZY)
    @ManyToMany(mappedBy = "projects")
    @OrderBy("name ASC")
    private Collection<Employee> employees=new ArrayList<Employee>();

    @Access(AccessType.PROPERTY)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Collection<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}

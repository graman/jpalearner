package basic.entities;

/**
 * Employee type can either be FULL/PART Time or Contract.
 */
public enum EmployeeType {
    FULLTIME,
    PARTTIME,
    CONTRACT
}

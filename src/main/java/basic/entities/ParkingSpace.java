package basic.entities;

import javax.persistence.*;

/**
 * Created by Ganesh on 19-05-2014.
 */
@SuppressWarnings({"MethodReturnOfConcreteClass", "PublicMethodNotExposedInInterface", "InstanceVariableOfConcreteClass", "CyclicClassDependency", "InstanceVariableMayNotBeInitialized"})
@Entity
@Table(name = "PRK_SPC")
public class ParkingSpace {

    @Id
    @Column(name = "PRK_SPC_ID")
    private int id;

    @OneToOne(mappedBy = "parkingSpace")
    private Employee employee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

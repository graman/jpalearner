package basic.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Ganesh on 20-05-2014.
 */
@SuppressWarnings({"PublicMethodNotExposedInInterface", "CyclicClassDependency", "ReturnOfCollectionOrArrayField"})
@Entity
@Table(name = "PRINT_QUEUE")
public class PrintQueue {

    @Id
    @Column(name ="Q_ID")private int id;
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "queue")
    @OrderColumn(name = "PRINT_EMP_ID")
    private Collection<PrintJob> jobs= Collections.EMPTY_LIST;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<PrintJob> getJobs() {
        return jobs;
    }

    public void setJobs(Collection<PrintJob> jobs) {
        this.jobs = jobs;
    }
}

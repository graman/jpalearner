package basic.entities;

import javax.persistence.*;

/**
 * Created by Ganesh on 20-05-2014.
 */
@SuppressWarnings({"MethodReturnOfConcreteClass", "PublicMethodNotExposedInInterface", "InstanceVariableOfConcreteClass", "CyclicClassDependency", "InstanceVariableMayNotBeInitialized"})
@Entity
@Table(name = "PRINT_JOB")
public class PrintJob {
    @Id
    @Column(name = "PRINT_ID")
    private int id;
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JoinColumn(name = "Q_ID")
    private PrintQueue queue;

    @ManyToOne
    @JoinColumn(name="PRINT_EMP_ID")
    private Employee employee;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public PrintQueue getQueue() {
        return queue;
    }

    public void setQueue(PrintQueue queue) {
        this.queue = queue;
    }
}

package basic.entities;

import javax.persistence.*;

/**
 * Created by Ganesh on 25-05-2014.
 */

@Entity
@Table(name = "PHONE_ENTRY")
public class PhoneEntry {

    @Id
    @Column(name = "PHN_ID")
    private int id;
    @Column(name = "PHN_NUM")
    private String number;
    @Column(name="PHN_TYPE")
    @Enumerated(EnumType.STRING)
    private PhoneType type;

    @ManyToOne
    @JoinColumn(name = "EMP_ID")
    private Employee employee;

    public PhoneEntry() {

    }

    public PhoneEntry(int id, String number, PhoneType type, Employee employee) {
        this.id = id;
        this.number = number;
        this.type = type;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "PhoneEntry{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                '}';
    }
}

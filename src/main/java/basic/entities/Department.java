package basic.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Ganesh on 19-05-2014.
 */
@SuppressWarnings({"PublicMethodNotExposedInInterface", "CyclicClassDependency", "ReturnOfCollectionOrArrayField", "InstanceVariableMayNotBeInitialized"})
@Entity
@Table(name = "DEPT")
public class Department {

    @Id
    @Column(name = "DEPT_ID")
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "DEPT_NAME")
    private DepartmentName departmentName;

    @OneToMany(mappedBy = "department")
    @Basic(fetch = FetchType.LAZY)
    private Collection<Employee> employees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Access(AccessType.PROPERTY)
    public DepartmentName getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Collection<Employee> employees) {
        this.employees = employees;
    }
}
